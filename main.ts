import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import {NutrientFact, Recipe} from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits
} from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */
const sortAttribute = (obj: any) => {
  const sortedObject = {};
  const sortedKeys = Object.keys(obj).sort();

  sortedKeys.forEach((key) => {
    sortedObject[key] = obj[key];
  });

  return sortedObject;
};

const calcCheapestRecipe = (recipe: Recipe) => {
  const cheapestRecipe = {
    cheapestCost: 0,
    nutrientsAtCheapestCost: {},
  };

  recipe.lineItems.forEach((lineItem) => {
    const productIngredients = GetProductsForIngredient(lineItem.ingredient);

    const cheapestCostPerItem = productIngredients
      .map((product) => {
        const nutrientFactInBaseUnits = product.nutrientFacts.map((nutrientFact) =>
          GetNutrientFactInBaseUnits(nutrientFact),
        );
        const costPerBaseUnits = product.supplierProducts.map((supplierProduct) =>
          GetCostPerBaseUnit(supplierProduct),
        );

        return {
          nutrientFactInBaseUnits: nutrientFactInBaseUnits,
          // get the cheapest cost in array
          cheapestCostPerBaseUnit: Math.min(...costPerBaseUnits),
        };
      })
      .reduce((cheapestCostItem, costItem) =>
        costItem.cheapestCostPerBaseUnit < cheapestCostItem.cheapestCostPerBaseUnit
          ? costItem
          : cheapestCostItem,
      );

    // merge cheapest cost per unit
    cheapestRecipe.cheapestCost +=
      cheapestCostPerItem.cheapestCostPerBaseUnit * lineItem.unitOfMeasure.uomAmount;

    // merge nutrient fact quantity amount to the cheapestRecipe
    cheapestCostPerItem.nutrientFactInBaseUnits.forEach((nutrientFact) => {
      if (!cheapestRecipe.nutrientsAtCheapestCost.hasOwnProperty(nutrientFact.nutrientName)) {
        cheapestRecipe.nutrientsAtCheapestCost[nutrientFact.nutrientName] = nutrientFact;
        return;
      }

      const cheapestNutrientFact: NutrientFact =
        cheapestRecipe.nutrientsAtCheapestCost[nutrientFact.nutrientName];
      cheapestNutrientFact.quantityAmount.uomAmount += nutrientFact.quantityAmount.uomAmount;
    });

    // sort the the nutrient facts
    cheapestRecipe.nutrientsAtCheapestCost = sortAttribute(cheapestRecipe.nutrientsAtCheapestCost);
  });

  return cheapestRecipe;
};

recipeData.forEach((recipe) => {
  recipeSummary[recipe.recipeName] = calcCheapestRecipe(recipe);
});
/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
